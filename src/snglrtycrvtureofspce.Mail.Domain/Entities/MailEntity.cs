﻿using snglrtycrvtureofspce.Core.Base.Infrastructure;

namespace snglrtycrvtureofspce.Mail.Domain.Entities;

public class MailEntity : IEntity
{
    #region IEntity
    
    public Guid Id { get; set; }
    
    public DateTime CreatedDate { get; set; }
    
    public DateTime ModificationDate { get; set; }
    
    #endregion
    
    public required string From { get; set; }
    
    public required string To { get; set; }
    
    public required string Subject { get; set; }
    
    public required string Body { get; set; }
    
    public bool IsBodyHtml { get; set; }
    
    public Guid UserId { get; set; }
}