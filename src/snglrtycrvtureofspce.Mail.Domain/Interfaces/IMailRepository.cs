﻿using snglrtycrvtureofspce.Mail.Domain.Entities;

namespace snglrtycrvtureofspce.Mail.Domain.Interfaces;

public interface IMailRepository
{
    Task CreateMailAsync(MailEntity mail, CancellationToken cancellationToken);
}