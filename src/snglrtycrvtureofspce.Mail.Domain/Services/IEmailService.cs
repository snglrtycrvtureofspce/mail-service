﻿namespace snglrtycrvtureofspce.Mail.Domain.Services;

public interface IEmailService
{
    Task SendEmailAsync(string to, string subject, string body, bool isBodyHtml, CancellationToken cancellationToken);
}