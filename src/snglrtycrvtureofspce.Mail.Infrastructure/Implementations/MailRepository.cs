﻿using snglrtycrvtureofspce.Mail.Domain.Entities;
using snglrtycrvtureofspce.Mail.Domain.Interfaces;
using snglrtycrvtureofspce.Mail.Infrastructure.Data;

namespace snglrtycrvtureofspce.Mail.Infrastructure.Implementations;

public class MailRepository(MailDbContext context) : IMailRepository
{
    public async Task CreateMailAsync(MailEntity mail, CancellationToken cancellationToken)
    {
        await context.Mails.AddAsync(mail, cancellationToken);
        await SaveChangesAsync(cancellationToken);
    }
    
    private async Task SaveChangesAsync(CancellationToken cancellationToken)
    {
        await context.SaveChangesAsync(cancellationToken);
    }
}