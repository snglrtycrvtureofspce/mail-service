﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Refit;
using snglrtycrvtureofspce.Core.Microservices.Core.JwtAuth;
using snglrtycrvtureofspce.Core.Microservices.Core.ServerMiddleware;
using snglrtycrvtureofspce.Mail.Domain.Interfaces;
using snglrtycrvtureofspce.Mail.Domain.Services;
using snglrtycrvtureofspce.Mail.Infrastructure.Data;
using snglrtycrvtureofspce.Mail.Infrastructure.Implementations;
using snglrtycrvtureofspce.Mail.Infrastructure.Services;
using snglrtycrvtureofspce.User.Api;

namespace snglrtycrvtureofspce.Mail.Infrastructure;

public static class DependencyInjection
{
    public static IServiceCollection AddInfrastructureServices(this IServiceCollection services,
        IConfiguration configuration)
    {
        var connectionString = Environment.GetEnvironmentVariable("DEPLOYCONNECTION");
        services.AddDbContext<MailDbContext>(options =>
        {
            if (connectionString != null) options.UseNpgsql(connectionString);
        });
        
        services.AddTransient(_ => new RefitSettings
        {
            ContentSerializer = new NewtonsoftJsonContentSerializer(new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            }),
            CollectionFormat = CollectionFormat.Multi
        });
        
        services.AddTransient(sc =>
            RestService.For<IUserApi>(
                new JwtHttpClient(configuration.GetMicroserviceHost("UserApi"),
                    sc.GetService<IHttpContextAccessor>()
                    ?? throw new InvalidOperationException()), sc.GetService<RefitSettings>()));

        services.AddScoped<IMailRepository, MailRepository>();
        services.AddTransient<IEmailService, EmailService>();
        
        return services;
    }
}