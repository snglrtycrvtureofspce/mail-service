﻿using System.Net;
using System.Net.Mail;
using snglrtycrvtureofspce.Mail.Domain.Services;

namespace snglrtycrvtureofspce.Mail.Infrastructure.Services;

public class EmailService : IEmailService
{
    public async Task SendEmailAsync(string to, string subject, string body, bool isBodyHtml, 
        CancellationToken cancellationToken)
    {
        var mailMessage = new MailMessage
        {
            From = new MailAddress(Environment.GetEnvironmentVariable("USERNAME") ?? string.Empty),
            Subject = subject,
            Body = body,
            IsBodyHtml = isBodyHtml
        };

        mailMessage.To.Add(to);

        using var smtpClient = new SmtpClient();
        smtpClient.Host = "smtp.gmail.com";
        smtpClient.Port = 587;
        smtpClient.EnableSsl = true;
        smtpClient.Credentials =
            new NetworkCredential(
                Environment.GetEnvironmentVariable("USERNAME"),
                Environment.GetEnvironmentVariable("PASSWORD")
            );

        await smtpClient.SendMailAsync(mailMessage, cancellationToken);
    }
}