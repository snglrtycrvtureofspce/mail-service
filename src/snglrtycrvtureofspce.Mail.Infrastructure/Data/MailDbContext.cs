﻿using Microsoft.EntityFrameworkCore;
using snglrtycrvtureofspce.Core.Base.Infrastructure;
using snglrtycrvtureofspce.Mail.Domain.Entities;

namespace snglrtycrvtureofspce.Mail.Infrastructure.Data;

public class MailDbContext : DbContext
{
    public virtual DbSet<MailEntity> Mails { get; init; }
    
    public MailDbContext(DbContextOptions<MailDbContext> opt) : base(opt) { }
    
    public MailDbContext() { }

    public override int SaveChanges()
    {
        ChangeTracker.DetectChanges();
        
        var added = ChangeTracker
            .Entries()
            .Where(w => w.State == EntityState.Added)
            .Select(s => s.Entity)
            .ToList();
        
        foreach (var entry in added)
        {
            if (entry is not IEntity entity)
            {
                continue;
            }
            
            entity.CreatedDate = DateTime.UtcNow;
            entity.ModificationDate = DateTime.UtcNow;
        }
        
        var updated = ChangeTracker
            .Entries()
            .Where(w => w.State == EntityState.Modified)
            .Select(s => s.Entity)
            .ToList();
        
        foreach (var entry in updated)
        {
            if (entry is IEntity entity)
            {
                entity.ModificationDate = DateTime.UtcNow;
            }
        }
        
        return base.SaveChanges();
    }
    
    public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new())
    {
        return Task.Run(SaveChanges, cancellationToken);
    }
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<MailEntity>(e =>
        {
            e.Property(x => x.From)
                .HasMaxLength(255)
                .IsRequired();
            
            e.Property(x => x.To)
                .HasMaxLength(255)
                .IsRequired();
            
            e.Property(x => x.Subject)
                .HasMaxLength(255)
                .IsRequired();
            
            e.Property(x => x.Body)
                .HasMaxLength(5000)
                .IsRequired();
        });
        
        base.OnModelCreating(modelBuilder);
    }
}