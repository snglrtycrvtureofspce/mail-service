﻿using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using snglrtycrvtureofspce.Core.Microservices.Core.ServerMiddleware;
using snglrtycrvtureofspce.Mail.Application.Handlers.MailController;
using Swashbuckle.AspNetCore.Annotations;

namespace snglrtycrvtureofspce.Mail.Api.Controllers;

[ApiController]
[Route(template: "[controller]")]
[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
[Produces(contentType: "application/json")]
public class MailController(ISender sender) : ControllerBase
{
    /// <summary>
    /// The method provider possibility to create mail.
    /// </summary>
    /// <param name="request">The request object containing the details of the mail to be created.</param>
    /// <returns>A newly created mail item.</returns>
    /// <remarks>
    /// Sample request:
    ///
    ///     POST /CreateMail
    ///     {
    ///        "to": "john.doe@example.com",
    ///        "subject": "Mail Subject",
    ///        "body": "Mail Body",
    ///        "isBodyHtml": true
    ///     }
    ///
    /// </remarks>
    /// <response code="201">Returns the newly created mail item.</response>
    [HttpPost(Name = "CreateMail")]
    [SwaggerResponse(statusCode: StatusCodes.Status201Created, type: typeof(CreateMailResponse))]
    public async Task<IActionResult> CreateMail([FromBody] CreateMailRequest request)
    {
        request.UserId = User.Claims.GetUserId();
        
        var response = await sender.Send(request);
        return Created("", response);
    }
}