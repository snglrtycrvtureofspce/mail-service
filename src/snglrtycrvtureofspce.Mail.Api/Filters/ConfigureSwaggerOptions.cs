﻿using System;
using System.Text;
using Asp.Versioning.ApiExplorer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace snglrtycrvtureofspce.Mail.Api.Filters;

/// <summary>
/// Configures the Swagger generation options for the Mail API.
/// </summary>
public class ConfigureSwaggerOptions(IApiVersionDescriptionProvider provider) : IConfigureOptions<SwaggerGenOptions>
{
    public void Configure(SwaggerGenOptions options)
    {
        foreach (var versionDescription in provider.ApiVersionDescriptions)
        {
            options.SwaggerDoc(versionDescription.GroupName, CreateInfoForApiVersion(versionDescription));
        }
    }
    
    private static OpenApiInfo CreateInfoForApiVersion(ApiVersionDescription description)
    {
        var descriptionBuilder = new StringBuilder("Mail API with OpenAPI, Swashbuckle, and API versioning.");
        
        if (description.IsDeprecated)
        {
            descriptionBuilder.Append(" This API version has been deprecated.");
        }
        
        if (description.SunsetPolicy?.Date.HasValue == true)
        {
            descriptionBuilder.Append($" The API will be sunset on {description.SunsetPolicy.Date.Value:yyyy-MM-dd}.");
        }
        
        if (description.SunsetPolicy?.HasLinks == true)
        {
            foreach (var link in description.SunsetPolicy.Links)
            {
                if (link.Type == "text/html")
                {
                    descriptionBuilder.AppendLine()
                        .Append(link.Title.Value ?? "Link")
                        .Append(": ")
                        .Append(link.LinkTarget);
                }
            }
        }
        
        return new OpenApiInfo
        {
            Title = "Mail API",
            Version = description.ApiVersion.ToString(),
            Description = descriptionBuilder.ToString(),
            Contact = new OpenApiContact
            {
                Name = "snglrtycrvtureofspce",
                Url = new Uri("https://snglrtycrvtureofspce.me"),
                Email = "snglrtycrvtureofspce@gmail.com"
            },
            License = new OpenApiLicense
            {
                Name = "MIT",
                Url = new Uri("https://gitlab.com/snglrtycrvtureofspce/mail-service/-/blob/main/LICENSE")
            }
        };
    }
}