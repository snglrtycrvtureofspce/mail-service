using Microsoft.AspNetCore.Builder;
using snglrtycrvtureofspce.Core.Middlewares;
using snglrtycrvtureofspce.Mail.Api;
using snglrtycrvtureofspce.Mail.Application;
using snglrtycrvtureofspce.Mail.Infrastructure;

var builder = WebApplication.CreateBuilder(args);

DotNetEnv.Env.Load();

builder.Services.AddApplicationServices(builder.Configuration);
builder.Services.AddInfrastructureServices(builder.Configuration);
builder.Services.AddApiServices(builder.Configuration);

/*builder.Services.AddRabbit(builder.Configuration);

if (Environment.GetEnvironmentVariable("DISABLE_RABBITMQ") == null)
{
    builder.Services.AddRabbitMqEndpoints(cfg =>
    {
        cfg.MapEndpoint<CreateMailRequest>(queue: "service.mail.post", durable: true, 
                exclusive: false, 
                autoDelete: false,
                arguments: null)
            .WithBinding("service.mail.mailpost", "mailpost.create");
    });
}*/

var app = builder.Build();

app.UseRouting();
app.UseCors("AllowAll");

app.UseSwagger();
app.UseSwaggerUI(options =>
{
    foreach (var description in app.DescribeApiVersions())
    {
        options.SwaggerEndpoint($"{description.GroupName}/swagger.json", description.GroupName.ToUpperInvariant());
    }
});

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.UseMiddleware<ExceptionHandlingMiddleware>();

app.Run();