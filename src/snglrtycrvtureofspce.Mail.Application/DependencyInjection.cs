﻿using System.Reflection;
using AutoMapper;
using FluentValidation;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using snglrtycrvtureofspce.Core.Filters;

namespace snglrtycrvtureofspce.Mail.Application;

public static class DependencyInjection
{
    public static IServiceCollection AddApplicationServices(this IServiceCollection services,
        IConfiguration configuration)
    {
        var applicationAssembly = Assembly.GetExecutingAssembly();
        
        var mapperConfig = new MapperConfiguration(p => p.AddMaps(Assembly.GetExecutingAssembly()));
        var mapper = mapperConfig.CreateMapper();
        services.AddSingleton(mapper);
        services.AddScoped<IMapperBase>(_ => mapper);
        
        services.AddMediatR(config => config.RegisterServicesFromAssembly(applicationAssembly));

        services.AddValidatorsFromAssembly(applicationAssembly);
        
        services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestValidationBehavior<,>));
        
        return services;
    }
}