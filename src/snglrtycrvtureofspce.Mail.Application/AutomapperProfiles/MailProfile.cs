﻿using AutoMapper;
using snglrtycrvtureofspce.Mail.Application.DTOs;
using snglrtycrvtureofspce.Mail.Domain.Entities;

namespace snglrtycrvtureofspce.Mail.Application.AutomapperProfiles;

public class MailProfile : Profile
{
    public MailProfile()
    {
        CreateMap<MailEntity, MailDto>();
    }
}