﻿using FluentValidation;

namespace snglrtycrvtureofspce.Mail.Application.Handlers.MailController;

public class CreateMailRequestValidator : AbstractValidator<CreateMailRequest>
{
    public CreateMailRequestValidator()
    {
        RuleFor(command => command.To)
            .MaximumLength(255).WithMessage("Email address has a maximum length of 255.")
            .NotNull().WithMessage("Email address cannot be null.")
            .NotEmpty().WithMessage("Email address cannot be empty.")
            .EmailAddress().WithMessage("A valid email address is required.");

        RuleFor(command => command.Subject)
            .MaximumLength(255).WithMessage("Subject has a maximum length of 255.")
            .NotNull().WithMessage("Subject cannot be null.")
            .NotEmpty().WithMessage("Subject cannot be empty.");

        RuleFor(command => command.Body)
            .MaximumLength(5000).WithMessage("Subject has a maximum length of 5000.")
            .NotNull().WithMessage("Body cannot be null.")
            .NotEmpty().WithMessage("Body cannot be empty.");

        RuleFor(command => command.IsBodyHtml)
            .NotNull().WithMessage("IsBodyHtml cannot be null.");

        RuleFor(command => command.UserId)
            .NotNull().WithMessage("UserId cannot be null.")
            .NotEmpty().WithMessage("UserId cannot be empty.");
    }
}