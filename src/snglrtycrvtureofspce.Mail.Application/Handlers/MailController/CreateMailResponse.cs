﻿using snglrtycrvtureofspce.Core.Base.Responses;
using snglrtycrvtureofspce.Mail.Application.DTOs;

namespace snglrtycrvtureofspce.Mail.Application.Handlers.MailController;

public class CreateMailResponse : ItemResponse<MailDto>;