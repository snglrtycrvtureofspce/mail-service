﻿using MediatR;

namespace snglrtycrvtureofspce.Mail.Application.Handlers.MailController;

public class CreateMailRequest : IRequest<CreateMailResponse>
{
    public required string To { get; set; }
    
    public required string Subject { get; set; }
    
    public required string Body { get; set; }
    
    public bool IsBodyHtml { get; set; }
    
    public Guid UserId { get; set; }
}