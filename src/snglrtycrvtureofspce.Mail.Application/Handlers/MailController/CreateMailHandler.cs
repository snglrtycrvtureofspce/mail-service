﻿using AutoMapper;
using MediatR;
using snglrtycrvtureofspce.Mail.Application.DTOs;
using snglrtycrvtureofspce.Mail.Domain.Entities;
using snglrtycrvtureofspce.Mail.Domain.Interfaces;
using snglrtycrvtureofspce.Mail.Domain.Services;

namespace snglrtycrvtureofspce.Mail.Application.Handlers.MailController;

public class CreateMailHandler(IMailRepository mailRepository, IEmailService emailService , IMapperBase mapper) : 
    IRequestHandler<CreateMailRequest, CreateMailResponse>
{
    public async Task<CreateMailResponse> Handle(CreateMailRequest request, CancellationToken cancellationToken)
    {
        var mail = new MailEntity
        {
            From = Environment.GetEnvironmentVariable("USERNAME")!,
            To = request.To,
            Subject = request.Subject,
            Body = request.Body,
            IsBodyHtml = request.IsBodyHtml,
            UserId = request.UserId
        };
        
        await mailRepository.CreateMailAsync(mail, cancellationToken);
        
        var model = mapper.Map<MailDto>(mail);
        
        await emailService.SendEmailAsync(request.To, request.Subject, request.Body, request.IsBodyHtml, 
            cancellationToken);
        
        var response = new CreateMailResponse
        {
            Message = "Mail have been successfully created.",
            Item = model
        };
        
        return response;
    }
}